<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%eventOrganization}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%events}}`
 * - `{{%organizations}}`
 */
class m221117_080316_create_eventOrganization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%eventOrganization}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'organization_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `event_id`
        $this->createIndex(
            '{{%idx-eventOrganization-event_id}}',
            '{{%eventOrganization}}',
            'event_id'
        );

        // add foreign key for table `{{%events}}`
        $this->addForeignKey(
            '{{%fk-eventOrganization-event_id}}',
            '{{%eventOrganization}}',
            'event_id',
            '{{%events}}',
            'id',
            'CASCADE'
        );

        // creates index for column `organization_id`
        $this->createIndex(
            '{{%idx-eventOrganization-organization_id}}',
            '{{%eventOrganization}}',
            'organization_id'
        );

        // add foreign key for table `{{%organizations}}`
        $this->addForeignKey(
            '{{%fk-eventOrganization-organization_id}}',
            '{{%eventOrganization}}',
            'organization_id',
            '{{%organizations}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%events}}`
        $this->dropForeignKey(
            '{{%fk-eventOrganization-event_id}}',
            '{{%eventOrganization}}'
        );

        // drops index for column `event_id`
        $this->dropIndex(
            '{{%idx-eventOrganization-event_id}}',
            '{{%eventOrganization}}'
        );

        // drops foreign key for table `{{%organizations}}`
        $this->dropForeignKey(
            '{{%fk-eventOrganization-organization_id}}',
            '{{%eventOrganization}}'
        );

        // drops index for column `organization_id`
        $this->dropIndex(
            '{{%idx-eventOrganization-organization_id}}',
            '{{%eventOrganization}}'
        );

        $this->dropTable('{{%eventOrganization}}');
    }
}
