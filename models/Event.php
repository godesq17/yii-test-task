<?php

namespace app\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property EventOrganization[] $eventOrganizations
 */
class Event extends \yii\db\ActiveRecord
{
    public $organizationIds = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['organizationIds'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    public function beforeSave($insert): bool
    {
        if (parent::beforeSave($insert)) {
            $this->updated_at = date('Y-m-d H:i:s');
            return true;
        }
        return false;
    }

    public function getEventOrganization()
    {
        return $this->hasMany(EventOrganization::class, ['event_id' => 'id']);
    }

    public function getOrganizations()
    {
        return $this->hasMany(Organization::class, ['id' => 'organization_id'])->via('eventOrganization');
    }

    public function getdropOrganization()
    {
        $data = Organization::find()->asArray()->all();
        return ArrayHelper::map($data, 'id', 'fio');
    }

    public function getOrganizationIds()
    {
        $this->organizationIds = \yii\helpers\ArrayHelper::getColumn(
            $this->getEventOrganization()->asArray()->all(),
            'organization_id'
        );
        return $this->organizationIds;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $organizationExists = 0;
        $organizationRemove = [];

        if (($actualOrganizations = EventOrganization::find()
                ->andWhere("event_id = $this->id")
                ->asArray()
                ->all()) !== null) {
            $actualOrganizations = $organizationRemove = ArrayHelper::getColumn($actualOrganizations, 'organization_id');
            $organizationExists = 1;
        }

        if (!empty($this->organizationIds)) {
            foreach ($this->organizationIds as $id) {
                $organizationRemove = array_diff($organizationRemove, [$id]);
                if (!in_array($id, $actualOrganizations)) {
                    $r = new EventOrganization();
                    $r->event_id = $this->id;
                    $r->organization_id = $id;
                    $r->save();
                }
            }
        }

        if ($organizationExists == 1) {
            foreach ($organizationRemove as $remove) {
                $r = EventOrganization::findOne(['organization_id' => $remove, 'event_id' => $this->id]);
                $r->delete();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getOrganizationListText() {
        $data = [];
        foreach ($this->organizations as $organization) {
            $data[] = $organization->fio . ' (' . $organization->email . ')';
        }
        return implode(",", $data);
    }
}
