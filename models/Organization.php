<?php

namespace app\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "organizations".
 *
 * @property int $id
 * @property string $fio
 * @property string $email
 * @property string|null $phone
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property EventOrganization[] $eventOrganizations
 */
class Organization extends \yii\db\ActiveRecord
{
    public $eventIds = [];
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'organizations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['fio', 'email'], 'required'],
            [['fio'], 'string'],
            [['email', 'phone'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['eventIds'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'fio' => 'Fio',
            'email' => 'Email',
            'phone' => 'Phone',
        ];
    }
    
    public function beforeSave($insert): bool
    {
       if (parent::beforeSave($insert)) {
           $this->updated_at = date('Y-m-d H:i:s');
           return true;
       }
       return false;
    }

    public function getEventOrganization()
    {
        return $this->hasMany(EventOrganization::class, ['organization_id' => 'id']);
    }

    public function getEvents()
    {
        return $this->hasMany(Event::class, ['id' => 'event_id'])->via('eventOrganization');
    }

    public function getdropEvent()
    {
        $data = Event::find()->asArray()->all();
        return ArrayHelper::map($data, 'id', 'name');
    }

    public function getEventIds()
    {
        $this->eventIds = \yii\helpers\ArrayHelper::getColumn(
            $this->getEventOrganization()->asArray()->all(),
            'event_id'
        );
        return $this->eventIds;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $eventExists = 0;
        $eventRemove = [];

        if (($actualEvents = EventOrganization::find()
                ->andWhere("organization_id = $this->id")
                ->asArray()
                ->all()) !== null) {
            $actualEvents = $eventRemove = ArrayHelper::getColumn($actualEvents, 'event_id');
            $eventExists = 1;
        }

        if (!empty($this->eventIds)) {
            foreach ($this->eventIds as $id) {
                $eventRemove = array_diff($eventRemove, [$id]);
                if (!in_array($id, $actualEvents)) {
                    $r = new EventOrganization();
                    $r->organization_id = $this->id;
                    $r->event_id = $id;
                    $r->save();
                }
            }
        }

        if ($eventExists == 1) {
            foreach ($eventRemove as $remove) {
                $r = EventOrganization::findOne(['event_id' => $remove, 'organization_id' => $this->id]);
                $r->delete();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getEventListText() {
        $data = [];
        foreach ($this->events as $event) {
            $data[] = $event->name;
        }
        return implode(",", $data);
    }
}
