<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>List of events</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Organization</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($events as $event): ?>
                    <tr>
                        <th scope="row"><?= $event->id ?></th>
                        <td><?= $event->name ?></td>
                        <td><?= $event->description ?></td>
                        <td>
                            <?php foreach ($event->organizations as $organization): ?>
                                <?= $organization->fio ?> <br>
                                <?= $organization->email ?> <br>
                                <?= $organization->phone ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
